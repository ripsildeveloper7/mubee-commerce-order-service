
var mongoose = require('mongoose');

const ReferAndEarnSchema = new mongoose.Schema({
    couponName: String,
    couponDescription: String,
    referrerPercentage: Number,
    applierPercetage: Number,
    countOfApplier: Number,
    startDate: Date,
    endDate: Date,
    createdDate: { type: Date, default: Date.now()},
    modifiedDate: { type: Date, default: Date.now()},
    customerCouponDetails: [{
        customerId: {type: mongoose.Schema.Types.ObjectId}, randomCoupon:  String
      }], 
    applier: Boolean
});

module.exports = ReferAndEarnSchema;