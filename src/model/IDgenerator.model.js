var mongoose = require('mongoose');

const IDgeneratorSchema = new mongoose.Schema({
 /*  lastDigit: {type: String, default: "10001"}, */
 lastDigit: String
});

const IDgenerator = mongoose.model('IDgenerator', IDgeneratorSchema);
module.exports = IDgenerator;