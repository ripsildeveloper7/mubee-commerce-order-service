var mongoose = require('mongoose');

const poSettingSchema = new mongoose.Schema({
    cgstRate: Number,
    sgstRate: Number,
    igstRate: Number,
    termsAndCondition: String,
    poType: [String],
    gstIn: String,
    contactNo: Number,
    billingAddress: String
});

const PoSetting = mongoose.model('posetting', poSettingSchema);
module.exports = PoSetting;