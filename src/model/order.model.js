var mongoose = require('mongoose');
var Product = require('./product.model');
var Cart = require('./cart.model');
var Coupon = require('./coupon.model');
var ReferAndEarn = require('./referAndEarn.model');
const OrderSchema = new mongoose.Schema({
    customerId: String,
    orderedMobileNumber: Number,
    orderId: String,
    items: [{
        productId: mongoose.Schema.Types.ObjectId, pack: Number, moq: Number,
        ratioQty: Number
    }],
    total: Number,
    addressDetails: [{
        name: String,
        mobileNumber: Number,
        streetAddress: String,
        building: String,
        landmark: String,
        city: String,
        state: String,
        pincode: String,
        country: String
    }],
    paymentStatus: String,
    orderStatus: String,
    orderDate: Date,
    orderUpdatedDate: Date,
    razorPayOrderId: String,
    paymentStatus: String,
    // product details
    orderedProducts: [
        Product
    ],
    // cart details
    cart: [
        Cart
    ],
    // payment details
    razorpayPaymentId: String,
    razorpaySignature: String,
    paymentStatus: String,
    paymentMode: String,
    shipmentStatus: String,
    paypalOrderId: String,
    walletAmount: Number,
    purchaseOrderStatus: String,
    // coupon details
    coupon: [Coupon],
    referAndEarn: ReferAndEarn
});
const Order = mongoose.model('orders', OrderSchema);
module.exports = Order;