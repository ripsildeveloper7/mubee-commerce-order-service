var poSettingMgr = require('./purchase-orderMgr');
/* var upload = require('../config/multer.config'); */

module.exports = function (app) {
    app.route('/getposetting').get(poSettingMgr.getPOsetting);
    app.route('/cgstsetting').post(poSettingMgr.CGSTsetting);
    app.route('/sgstsetting').post(poSettingMgr.SGSTsetting);
    app.route('/igstsetting').post(poSettingMgr.IGSTsetting);
    app.route('/gstinsetting').post(poSettingMgr.GSTINsetting);
    app.route('/potypesetting').post(poSettingMgr.POTypeSetting);
    app.route('/termsandconditionsetting').post(poSettingMgr.TermsAndConditionsetting);
    app.route('/addcontactnosetting').post(poSettingMgr.ContactNosetting);
    app.route('/deletepotypesetting/:poType').delete(poSettingMgr.deletePOType);
    app.route('/addbillingaddress').post(poSettingMgr.billingAddress);
    app.route('/sendpdfforemail').post(poSettingMgr.sendMailWithPdf);
    app.route('/createpurchaseorder').post(poSettingMgr.createPurchaseOrder);
    app.route('/getallpurchaseorder').get(poSettingMgr.getAllPurchaseOrder);
    app.route('/getallpurchaseordercount').get(poSettingMgr.getAllPurchaseOrderCount);
}