var poSetting = require('../model/poSetting.model');
var purchaseOrder = require('../model/purchaseOrder.model');

exports.getPOsetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}
exports.CGSTsetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].cgstRate = req.body.cgstRate;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}

exports.SGSTsetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].sgstRate = req.body.sgstRate;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}
exports.IGSTsetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].igstRate = req.body.igstRate;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}

exports.TermsAndConditionsetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].termsAndCondition = req.body.termsAndCondition;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}
exports.GSTINsetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].gstIn = req.body.gstIn;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}
exports.ContactNosetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].contactNo = req.body.contactNo;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}

exports.POTypeSetting = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                var element = req.body.poType;
                var list = findData[0].poType.indexOf(element);
                if (list > -1) {
                    res.status(200).send({
                        "result": "Already Exist"
                    });
                } else {
                    findData[0].poType.push(req.body.poType);
                    findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
                }
            }
        }
    })
}
exports.deletePOType = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var element = req.params.poType;
            var list = findData[0].poType;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
}

exports.billingAddress = function(req, res) {
    poSetting.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new poSetting(req.body);
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].billingAddress = req.body.billingAddress;
                findData[0].save(function(err, updateData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(updateData);
                    }
                })
            }
        }
    })
} 

exports.createPurchaseOrder = function(req, res, num) {
    var poData = [];
    var code = 'BTB';
    var currentDate = new Date();
    var year = currentDate.getFullYear();
    for (let i = 0; i <= req.body.length - 1; i ++) {
        var po = [];
        po[i] = new purchaseOrder();
        po[i].poType = req.body[i].poType;
        po[i].poDate = req.body[i].poDate; 
        po[i].poExpiryDate = req.body[i].poExpiryDate;
        po[i].billTo = req.body[i].billTo;
        po[i].gstNo = req.body[i].gstNo;
        po[i].contactNo = req.body[i].contactNo;
        po[i].vendorName = req.body[i].vendorName;
        po[i].vendorId = req.body[i].vendorId;
        po[i].vendorAddress = req.body[i].vendorAddress;
        po[i].vendorEmailId = req.body[i].vendorEmailId;
        po[i].vendorCSTNo = req.body[i].vendorCSTNo;
        po[i].vendorGStNo = req.body[i].vendorGStNo;
        po[i].vendorCurrencyType = req.body[i].vendorCurrencyType;
        po[i].netAmount = req.body[i].netAmount;
        po[i].freight = req.body[i].freight;
        po[i].tax = req.body[i].tax;
        po[i].vendorItems = req.body[i].vendorItems;
        po[i].specialInstruction = req.body[i].specialInstruction;
        po[i].totalAmount = req.body[i].totalAmount;
        po[i].orderId = req.body[i].orderId;
        var result = num + (i + 1);
        po[i].poNumber = code + '/' + year + '/' + result;
        poData.push(po[i]);
    }
    purchaseOrder.insertMany(poData, function(err, insertData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(insertData);
        }
    })
}

exports.getAllPurchaseOrder = function(req, res) {
    purchaseOrder.find({}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.getAllPurchaseOrderCount = function(req, res) {
    purchaseOrder.find({}).count().exec(function(err,findData){
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

